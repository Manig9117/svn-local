module github.com/xinliangnote/go-gin-api

go 1.15

require (
	github.com/99designs/gqlgen v0.13.0
	github.com/StackExchange/wmi v0.0.0-20210224194228-fe8f1750fd46 // indirect
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/dave/dst v0.26.2
        github.com/djherbis/times v1.4.0
	)
